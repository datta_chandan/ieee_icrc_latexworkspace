# About IEEE ICRC 2017

The First IEEE International Conference on Robotic Computing:  http://icrc.asia.edu.tw/paper-submission/
%Submission: https://easychair.org/account/signin.cgi?key=45411984.DtLIU4SMkKu2taTb 

1. Paper Submission	January 27, 2017
2. Acceptance Notification	February 7, 2017
3. Camera-ready and Registration	February 15, 2017
4. Conference	April 10-12, 2017	

# Useful links

- http://icrc.asia.edu.tw/ss-rsa/
- https://ras.papercept.net/conferences/scripts/submissionwizard.pl
- https://ras.papercept.net/conferences/scripts/start.pl
- IEEE eXpress compliance: http://ras.papercept.net/conferences/scripts/pdftest.pl


# Changelog

- Revised the paper from 6 pages to 4 pages
	- Removed `RoboStudio platform core classes` section
	- Removed Figure 3 to address reviewers:
		- > Figure 3 is only mentioned in the paper and there is not any explanation of the blocks ans etc in the figure.
- Added text to the abstract to clearly identify the contributions  and it's importance to address reviewers:

	- > Why is such a detailed description of the implementation important and why can it be interested for readers? The reviewer found the previous publication of the RoboStudio (Ref. 6) more interesting and informative for the readers. What is the contribution of this paper?
	- > The DSL and the visual programming environment has been already described in [6] and [7], the novelty introduced in this paper are not clearly stated
	- > Although the paper includes an "Introduction and related work" section at the beginning, related works are not properly described and compared with the authors' research, making it difficult to grasp their  contribution and the benefits/limitations of their proposal.
- Added higher level description  and how it works in section I to address reviewers:
	- > the paper goes too much in the details of how the tool is implemented, and does not give enough space to a higher level description of how the user can use the tool to implement generic robot behaviors
	- > I think the paper would stand a better chance to convince the reader if it had a running "small" complete example showing how the studio is used and how it helps setting things up for the final programmer/user.
	- > Figure 1 is not explained in the paper
- Added text to the `related work` section to address reviewers:
> I would recommend including a "related works" section at the end of the paper to describe related researches a bit more in detail, allowing the comparison with the authors' proposal once it has been described in the previous sections.
> Although the paper includes an "Introduction and related work" section at the beginning, related works are not properly described and compared with the authors' research, making it difficult to grasp their  contribution and the benefits/limitations of their proposal.
- Added text to confirm that the service applications were run in the real robot to test semantic equivalence
> The authors claim that "the code generated using RoboStudio represented the same application logic as the hand-coded scripts and hence confirmed the validity of the xml code generation". How is this "semantic" equivalence verified/guarantied?




# Submission instructions


```

Dear author / Chandan Datta,

Congratulations! It is our pleasure to inform you that your paper submitted to IEEE IRC 2017 entitled as follows,


---------------------------------------------
#48 Architecture of an extensible visual programming environment for authoring behaviour of personal service robots
---------------------------------------------

has been Accepted as a short paper ([4] pages).

Please consider to address the comments from reviewers to the camera-ready version of your paper before final submission.

To make sure your paper is published by the conference proceedings on time, please follow the IMPORTANT NOTES to prepare the camera-ready (final version) of
your paper and complete the conference registration by February 15, 2017 PST.

Please note that at least one author of each paper has a full registration and attend the conference for presenting the paper.

*IMPORTANT NOTES*

Submit the camera-ready to IEEE conference publishing services (CPS) --- Online Author Kit

  URL: http://icrc.asia.edu.tw/paper-submission/


Complete the conference registration (online)

 Please make sure that at least one author of each paper completes a full registration at the following URL.

URL: http://icrc.asia.edu.tw/registration/
Deadline of registration: February 15, 2017 PST

 Please notice that the accepted papers without completing registration will not be included in the conference proceedings or the IEEE Xplore Digital Library.

Please contact us if you have any question about the decision and the preparation of the camera-ready paper.

We are looking forward to seeing you at the conference.


Sincerely,
--

Program Co-Chairs
Chun-Ming Chang, Asia University, Taiwan (cmchang@asia.edu.tw) |
Jianquan Liu, NEC, Japan |
Bruce MacDonald, University of Auckland, New Zealand |
Adriana Tapus, ENSTA ParisTech, France

```